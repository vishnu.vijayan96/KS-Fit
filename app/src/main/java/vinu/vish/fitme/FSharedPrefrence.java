package vinu.vish.fitme;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

/**
 * Created by vishnu on 25/12/16.
 */

public class FSharedPrefrence {

    static final String PREF_USER_EMAIL="user_google_email";
    static final String PREF_USER_DISPLAYNAME="user_google_displayname";
    static final String PREF_USER_SIGN_STATUS="user_google_sign_status";
    static final String PREF_USER_SIGN_TOKEN = "user_google_sign_token";

    private String USER_EMAIL;
    private String USER_DISPLAYNAME;
    private String USER_SIGN_TOKEN;

    static SharedPreferences getSharedPreferences(Context ctx) {
        return PreferenceManager.getDefaultSharedPreferences(ctx);
    }

    public static void setUserInfo(Context ctx, GoogleSignInAccount googleSignInAccount){
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_USER_EMAIL, googleSignInAccount.getEmail());
        editor.putString(PREF_USER_DISPLAYNAME, googleSignInAccount.getDisplayName());
        editor.putString(PREF_USER_SIGN_TOKEN, googleSignInAccount.getIdToken());
        editor.putInt(PREF_USER_SIGN_STATUS,1);
        editor.commit();
    }

    public static Boolean isSignedIn(Context ctx){
        int status =  getSharedPreferences(ctx).getInt(PREF_USER_SIGN_STATUS, 0);
        if(status == 1) {return true; } else {return false;}
    }

    public static void logOut (Context ctx){
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putInt(PREF_USER_SIGN_STATUS,0);
        editor.commit();
    }
}
