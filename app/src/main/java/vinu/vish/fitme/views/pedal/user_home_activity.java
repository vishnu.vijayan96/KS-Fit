package vinu.vish.fitme.views.pedal;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

import vinu.vish.fitme.R;
import vinu.vish.fitme.models.FitSingleton;

/**
 * Created by vishnu on 22/12/16.
 */

public class user_home_activity extends AppCompatActivity {
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private NetworkImageView profilePic ;
    private Toolbar toolbar;
    private TextView userName;
    FitSingleton google_acc;
    GoogleSignInAccount googleAcc_details;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_home_activity);
        bindResources();
        setUpToolbar();
        googleAcc_details = FitSingleton.getInstance(this).getGoogleaccount();
        setUPdrawer();
    }

    void bindResources(){
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        profilePic = (NetworkImageView) navigationView.getHeaderView(0).findViewById(R.id.profilePic);
        userName = (TextView) navigationView.getHeaderView(0).findViewById(R.id.userName);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
    }

    void setUpToolbar(){
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null){
            //actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

    }

    void setUPdrawer(){
        userName.setText(googleAcc_details.getDisplayName());
        ImageLoader imageLoader = FitSingleton.getInstance(this).getImageLoader();
        profilePic.setImageUrl(googleAcc_details.getPhotoUrl().toString(),imageLoader);

    }

}
