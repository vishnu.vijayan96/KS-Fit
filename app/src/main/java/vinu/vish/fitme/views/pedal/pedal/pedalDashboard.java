package vinu.vish.fitme.views.pedal.pedal;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import vinu.vish.fitme.R;

/**
 * Created by vishnu on 17/12/16.
 */

public class pedalDashboard extends AppCompatActivity {
    TextView valueTextView,timeTextView;
    Button resetButton;
    int steps;
    private long timestamp;
    private SensorManager sensormanager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pedaldashboard);
        timeTextView = (TextView)  findViewById(R.id._time);
        valueTextView = (TextView) findViewById(R.id.valueTextView);
        resetButton = (Button) findViewById(R.id._reset);
        steps = 0;
        try {
            registerSensorEvents();
        }catch (Exception e){
            Log.e("Sensor Error",e.getMessage());
        }

        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                steps=0;
                valueTextView.setText((int)steps+"");
            }
        });

    }

    void registerSensorEvents() throws Exception{

        //STEP COUNTER
        sensormanager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
        sensormanager.registerListener(new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {
                steps++;
                float tottalSteps = event.values[0];
                valueTextView.setText((int)steps+"");

            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {

            }
        },sensormanager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER),sensormanager.SENSOR_DELAY_UI);

        //STEP DETECTOR
        sensormanager.registerListener(new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {
                timestamp = event.timestamp / 1000000;
                timeTextView.setText(DateUtils.getRelativeTimeSpanString(timestamp));
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int accuracy) {

            }
        },sensormanager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR),SensorManager.SENSOR_DELAY_UI);

    }
}
