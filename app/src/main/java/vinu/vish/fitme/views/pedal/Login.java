package vinu.vish.fitme.views.pedal;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.auth.api.credentials.CredentialRequest;
import com.google.android.gms.auth.api.credentials.CredentialRequestResult;
import com.google.android.gms.auth.api.credentials.IdentityProviders;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.OptionalPendingResult;
import com.google.android.gms.common.api.ResolvingResultCallbacks;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import vinu.vish.fitme.FSharedPrefrence;
import vinu.vish.fitme.R;
import vinu.vish.fitme.models.FitSingleton;
import vinu.vish.fitme.views.pedal.pedal.pedalDashboard;

public class Login extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener,View.OnClickListener
,GoogleApiClient.ConnectionCallbacks{

    static final String TAG = "HomeActivity";


    SignInButton signInButton;
    GoogleApiClient mGoogleApiClient;
    CredentialRequest mCredintialRequest;
    private ProgressDialog mProgressDialog;
    private boolean mIsResolving = false;
    Credential mCredintialTosave;
    Credential mCredintial;


    private static final int RC_SIGN_IN = 1;
    private static final int RC_CREDENTIALS_READ = 2;
    private static final int RC_CREDENTIALS_SAVE = 3;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        bindResource();
        buildGoogleApiClient(null);

    }

    @Override
    protected void onStart() {
        super.onStart();
        requestCredintials(true,true);

    }

    void bindResource(){
        signInButton = (SignInButton) findViewById(R.id.google_sign_in_button);
        signInButton.setSize(SignInButton.SIZE_WIDE);
        signInButton.setOnClickListener(this);
    }

    private  void requestCredintials(final boolean shouldResolve, boolean onlyPasswords){
        CredentialRequest.Builder crBuilder = new CredentialRequest.Builder()
                .setPasswordLoginSupported(true);

        if(!onlyPasswords){
            crBuilder.setAccountTypes(IdentityProviders.GOOGLE);
        }

        showProgress();
        Auth.CredentialsApi.request(mGoogleApiClient , crBuilder.build()).setResultCallback(new ResultCallback<CredentialRequestResult>() {
            @Override
            public void onResult(@NonNull CredentialRequestResult credentialRequestResult) {
                hideProgress();
                Status status = credentialRequestResult.getStatus();
                if(status.isSuccess()){

                }else if(status.getStatusCode() == CommonStatusCodes.RESOLUTION_REQUIRED && shouldResolve){
                    // Getting credential needs to show some UI, start resolution
                }
            }
        });
    }

    private void resolveResult(Status status ,int requestCode){
        if(!mIsResolving){
            try{
                status.startResolutionForResult(Login.this,requestCode);
            } catch (IntentSender.SendIntentException e) {
                Log.e(TAG, "Failed to send Credentials intent.", e);
                mIsResolving = false;
            }
        }

    }


    private void buildGoogleApiClient (String accountname){
        GoogleSignInOptions.Builder gsoBuilder= new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail();

        if(accountname !=null){
            gsoBuilder.setAccountName(accountname);
        }

        if(mGoogleApiClient != null){
            mGoogleApiClient.stopAutoManage(this);
        }

        GoogleApiClient.Builder builder = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addApi(Auth.CREDENTIALS_API)
                .addApi(Auth.GOOGLE_SIGN_IN_API,gsoBuilder.build())
                .enableAutoManage(this,this);

        mGoogleApiClient = builder.build();
    }







    @Override
    public void onClick(View v) {
        switch(v.getId()) {

            case R.id.google_sign_in_button:
                signIn();
                break;
        }
    }



    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult:" + requestCode + ":" + resultCode + ":" + data);

        if(requestCode == RC_SIGN_IN){
            GoogleSignInResult gsr = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleGoogleSignin(gsr);
        }else if (requestCode == RC_CREDENTIALS_READ){
            mIsResolving = false;

            if (resultCode == RESULT_OK){
                Credential credintial = data.getParcelableExtra(Credential.EXTRA_KEY);
                handleCredintials(credintial);
            }
        } else if (requestCode == RC_CREDENTIALS_SAVE){
            mIsResolving = false;

            if (resultCode == RESULT_OK) {
                Toast.makeText(this, "Saved", Toast.LENGTH_SHORT).show();
            } else {
                Log.w(TAG, "Credential save failed.");
            }
        }

    }

    private void handleCredintials(Credential credential){
        mCredintial = credential;

        Log.d(TAG, "handlecredintial:" + credential.getAccountType() +":" + credential.getId());
        if (IdentityProviders.GOOGLE.equals(credential.getAccountType())){
            // Google account, rebuild GoogleApiClient to set account name and then try
            buildGoogleApiClient(credential.getId());
            googleSilentSignIn();
        }else {

        }
    }

    private void handleGoogleSignin(GoogleSignInResult gsr){
        boolean isSignedIn = (gsr != null) && gsr.isSuccess();
        if(isSignedIn){
            //Display signed-UI
            GoogleSignInAccount gsa = gsr.getSignInAccount();
            String status = String.format("Signed in as %s (%s)",gsa.getEmail(),gsa.getDisplayName());
            //((TextView) findViewById(R.id.text_google_status)).setText(status);

            //Save to Credintial API
            Credential credential = new Credential.Builder(gsa.getEmail())
                    .setAccountType(IdentityProviders.GOOGLE)
                    .setName(gsa.getDisplayName())
                    .setProfilePictureUri(gsa.getPhotoUrl())
                    .build();

            saveCredintialIfConnected(credential);

        }
    }

    private void googleSilentSignIn(){
        // Try silent sign-in with Google Sign In API
        OptionalPendingResult<GoogleSignInResult> opr =
                Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
        if (opr.isDone()){
            GoogleSignInResult gsr = opr.get();
            handleGoogleSignin(gsr);
        }else{
            showProgress();
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(@NonNull GoogleSignInResult googleSignInResult) {
                    hideProgress();
                    handleGoogleSignin(googleSignInResult);
                }
            });
        }
    }

    private void saveCredintialIfConnected(Credential credential){
        if (credential == null){
            return;
        }

        mCredintialTosave = credential;
        if (mGoogleApiClient.isConnected()) {
            Auth.CredentialsApi.save(mGoogleApiClient,mCredintialTosave).setResultCallback(
                    new ResolvingResultCallbacks<Status>(this,RC_CREDENTIALS_SAVE) {
                        @Override
                        public void onSuccess(@NonNull Status status) {
                            Log.d(TAG, "save:SUCCESS:" + status);
                            mCredintialTosave = null;
                            loadUserHome();
                        }

                        @Override
                        public void onUnresolvableFailure(@NonNull Status status) {
                            Log.d(TAG, "save:FAILED:" + status);
                            mCredintialTosave = null;
                        }
            });
        }

    }

    void loadUserHome(){
        Intent userhomeIntent = new Intent(this,user_home_activity.class);
        startActivity(userhomeIntent);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    private void showProgress() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setMessage("Loading...");
        }

        mProgressDialog.show();
    }

    private void hideProgress() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
