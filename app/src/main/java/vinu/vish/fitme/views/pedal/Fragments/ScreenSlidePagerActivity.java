package vinu.vish.fitme.views.pedal.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;

import vinu.vish.fitme.views.pedal.Fragments.animation.ZoomOutPageTransformer;
import vinu.vish.fitme.R;

/**
 * Created by vishnu on 21/12/16.
 */

public class ScreenSlidePagerActivity extends FragmentActivity {
    private ViewPager mPager;
    ScreenSliderPageAdapter pageAdapter;
    private int NUMER_OF_FRAGMENTS = 2;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen_slide_activity);
        mPager = (ViewPager)findViewById(R.id.pager);
        mPager.setPageTransformer(true, new ZoomOutPageTransformer());
        pageAdapter = new ScreenSliderPageAdapter(getSupportFragmentManager());
        mPager.setAdapter(pageAdapter);
    }

    private class ScreenSliderPageAdapter extends FragmentStatePagerAdapter{

        public ScreenSliderPageAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            switch (position){
                case 0:
                    fragment = new HeightFragmentSlide();
                    break;
                case 1:
                    fragment = new WeightFragmentSlide();
                    break;
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return NUMER_OF_FRAGMENTS;
        }
    }
}
