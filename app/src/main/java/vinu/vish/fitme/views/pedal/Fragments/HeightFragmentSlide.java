package vinu.vish.fitme.views.pedal.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import vinu.vish.fitme.R;

/**
 * Created by vishnu on 21/12/16.
 */

public class HeightFragmentSlide extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.height_fragment_slide,container,false);
        return rootView;
    }
}
