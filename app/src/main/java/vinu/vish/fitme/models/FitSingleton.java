package vinu.vish.fitme.models;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.support.v4.util.LruCache;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

/**
 * Created by vishnu on 24/12/16.
 */
public class FitSingleton {
    private GoogleSignInAccount googleaccount;
    SharedPreferences sharedPref;
    private static FitSingleton mInstance;
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;
    //private Boolean SignedInStatus;
    private static Context mCtx;

    private FitSingleton(Context context) {
        mCtx = context;
        mRequestQueue = getRequestQueue();

        mImageLoader = new ImageLoader(mRequestQueue,
                new ImageLoader.ImageCache() {
                    private final LruCache<String, Bitmap>
                            cache = new LruCache<String, Bitmap>(20);

                    @Override
                    public Bitmap getBitmap(String url) {
                        return cache.get(url);
                    }

                    @Override
                    public void putBitmap(String url, Bitmap bitmap) {
                        cache.put(url, bitmap);
                    }
                });
    }

    public static synchronized FitSingleton getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new FitSingleton(context);

        }
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            mRequestQueue = Volley.newRequestQueue(mCtx.getApplicationContext());
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }

    public ImageLoader getImageLoader() {
        return mImageLoader;
    }



    public GoogleSignInAccount getGoogleaccount() {
        return googleaccount;
    }

    public void setGoogleaccount(GoogleSignInAccount googleaccount) {
        this.googleaccount = googleaccount;
    }


}
